package com.itau.Saque;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class FileManager {

	public static ArrayList<String> lerArquivoContas() {
		Path path = Paths.get("accounts.csv");
		ArrayList<String> linhas = new ArrayList<String>();

		try {
			linhas = (ArrayList<String>) Files.readAllLines(path);
			return linhas;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	public static ArrayList<String> lerArquivoSaques(){
		Path path = Paths.get("withdrawals.csv");
		ArrayList<String> linhas = new ArrayList<String>();

		try {
			linhas = (ArrayList<String>) Files.readAllLines(path);
			return linhas;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	public static void gravarArquivoContas(ArrayList<String> arquivo) {
		Path path = Paths.get("conta.csv");
		try {
			Files.write(path, arquivo);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}


