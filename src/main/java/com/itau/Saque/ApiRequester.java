package com.itau.Saque;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;

public class ApiRequester {
	
	public static double obterIndiceDeConversao(String moeda)
	{
		Gson gson = new Gson();
		String response = HttpRequest.get("http://data.fixer.io/api/latest?access_key=578ac533a860ac0f3d508e3dbad0ef57").body();
		//		   	System.out.println("Response was: " + response);
		Fixer fixer = gson.fromJson(response, Fixer.class);
		return fixer.rates.get(moeda);
	}
}

