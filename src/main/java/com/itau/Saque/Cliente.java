package com.itau.Saque;

import java.text.DecimalFormat;

public class Cliente {
	
	String nome;
	String username;
	String password;
	double valorDisponivel;
	static String tipoConta;
	
	public Cliente(String linha) {
		
		String[] partes = linha.split(",");
		nome = partes[0];
		username = partes[1];
		password = partes[2];
		valorDisponivel = Double.parseDouble(partes[3]);
		tipoConta = partes[4];
		
	}
	
	public String toString() {
		
		DecimalFormat df = new DecimalFormat("0.##");
		return String.format("%s,%s,%s,%s,%s", nome, username, password, df.format(valorDisponivel).replace(",", "."), tipoConta);
		
	}
	
	public boolean sacarDinheiro(double valorSaque, String moeda) {
		double indiceConversao = obterIndiceConversao(moeda);
		double taxa = obterTaxa();
		double valorSaqueFinal = calcularValorSaqueConvertido(valorSaque, taxa, indiceConversao);
		if(valorDisponivel >= valorSaqueFinal) {
			valorDisponivel -= valorSaqueFinal;
			return true;
		} else {
			return false;
		}
	}
	
	public static double calcularValorSaqueConvertido(double valorSaque, double taxa, double indiceConversao) {

		return (valorSaque/indiceConversao) * (1 + taxa/100);

	}
	
	private static double obterIndiceConversao(String moeda) {
		return ApiRequester.obterIndiceDeConversao(moeda);
	}
	
	private static double obterTaxa() {
		
		if(tipoConta.equals("basic")) {
			return 2.5;
		} else {
			return 0;
		}
		
	}

}
